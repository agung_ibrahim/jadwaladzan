package com.agung.jadwaladzan.utils

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat
import com.agung.jadwaladzan.R

class MyService : Service() {

    var count: Int = 0

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        onTaskRemoved(intent)
        count++
        Toast.makeText(applicationContext, "This is a Service running in Background", Toast.LENGTH_SHORT).show()
        Log.e("Running", "Gas")
        Log.e("counter", "$count")

        return START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()

        val broadcastIntent = Intent("ActivityRecognition.RestartSensor")
        sendBroadcast(broadcastIntent)
        Log.e("hancur", "123")
    }

    override fun onBind(intent: Intent?): IBinder {
        // TODO: Return the communication channel to the service.
        throw UnsupportedOperationException("Not yet implemented")
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        val restartServiceIntent = Intent(applicationContext, this.javaClass)
        restartServiceIntent.setPackage(getPackageName())
        startService(restartServiceIntent)
        super.onTaskRemoved(rootIntent)
    }
}