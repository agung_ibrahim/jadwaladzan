package com.agung.jadwaladzan.utils

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.agung.jadwaladzan.R

open class BaseFragment : Fragment() {
    lateinit var builder: AlertDialog.Builder
    internal var pd: LoadingDialog? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.let {
            builder = AlertDialog.Builder(it)
        }

        activity?.let {
            pd = LoadingDialog(it)
        }
    }

    fun showDialog(show: Boolean) {
        if(activity!= null) {
            if (null != pd)
                if (show)
                    pd?.show()
                else
                    pd?.dismiss()

            pd?.setCancelable(true)
        }
    }

    override fun onDestroy() {
        if(null!=pd) pd?.let { if(it.isShowing) it.dismiss() }
        super.onDestroy()
    }
}
