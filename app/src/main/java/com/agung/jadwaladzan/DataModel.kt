package com.agung.jadwaladzan

class DataModel {
    var tanggal: String? = null
    var jadwalAdzan : JadwalAdzan? = null

    class JadwalAdzan {
        var subuh: String? = null
        var terbit: String? = null
        var dzuhur: String? = null
        var asar: String? = null
        var maghrib: String? = null
        var isya: String? = null
    }
}