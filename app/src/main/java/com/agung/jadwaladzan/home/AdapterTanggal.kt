package com.agung.jadwaladzan.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.agung.jadwaladzan.DataModel
import com.agung.jadwaladzan.R
import kotlinx.android.synthetic.main.item_recycleview_calendar_layout.view.*
import timber.log.Timber
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class AdapterTanggal(private val context: Context, val page: HomePage) : RecyclerView.Adapter<AdapterTanggal.ItemHolder>() {

    val listData : ArrayList<DataModel> = ArrayList()
    var lokal: Locale? = null

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): AdapterTanggal.ItemHolder {
        return ItemHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_recycleview_calendar_layout, viewGroup, false))
    }

    override fun onBindViewHolder(holder: AdapterTanggal.ItemHolder, position: Int) {
        val item = listData[position]

        val indonesia = Locale("id","ID","ID")
        val currentTimestamp: String = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Date())

        val timestamp = Timestamp(System.currentTimeMillis())

        Timber.e("date $timestamp")
        val date = SimpleDateFormat("dddd-MM-yyyy").parse(item.tanggal)

        val dateFormat = SimpleDateFormat("dd")
        val hari = SimpleDateFormat("EEEE", indonesia).format(date)
        val tanggal = dateFormat.format(date)
        holder.textHari.text = hari.toString()
        holder.textTanggal.text = tanggal.toString()
        holder.textPosition.text = (position + 1).toString()

        holder.itemView.setOnClickListener {
            item.jadwalAdzan?.let { it1 -> page.setAdzan(it1) }
        }
    }

    override fun getItemCount(): Int {
        return listData.size
    }

    inner class ItemHolder(v: View) : RecyclerView.ViewHolder(v){
        var textHari = v.text_hari
        var textTanggal = v.text_tanggal
        var textPosition = v.text_position
    }

    fun updateList(list: List<DataModel>) {
        listData.clear()
        listData.addAll(list)
        notifyDataSetChanged()
    }

}