package com.agung.jadwaladzan.home

import android.R
import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.agung.jadwaladzan.DataModel
import com.agung.jadwaladzan.MainActivity
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import java.text.SimpleDateFormat
import java.util.*


class AppReceiver : BroadcastReceiver() {
    private var pendingIntent: PendingIntent? = null
    private val ALARM_REQUEST_CODE = 134

    private val interval_seconds = 1
    private var alarmNotificationManager: NotificationManager? = null
    var NOTIFICATION_CHANNEL_ID = "jadwalAdzan_id"
    var NOTIFICATION_CHANNEL_NAME = "jadwalAdzan channel"
    private val NOTIFICATION_ID = 1
    override fun onReceive(context: Context, intent: Intent) {
        val alarmIntent = Intent(context, AppReceiver::class.java)
        pendingIntent = PendingIntent.getBroadcast(context, ALARM_REQUEST_CODE, alarmIntent, 0)

        val cal: Calendar = Calendar.getInstance()
        cal.add(Calendar.SECOND, interval_seconds)
        val manager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        if (Build.VERSION.SDK_INT >= 23) {
            manager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent)
        } else if (Build.VERSION.SDK_INT >= 19) {
            manager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent)
        } else {
            manager[AlarmManager.RTC_WAKEUP, cal.getTimeInMillis()] = pendingIntent
        }

        val sp = context.getSharedPreferences("cekData", Context.MODE_PRIVATE)
        val dataAdzan = sp?.getString("dataList", "DEFAULT").toString()
        if (dataAdzan.isNotEmpty()) {
            val jsonParser = JsonParser()
            val data : JsonObject = jsonParser.parse(dataAdzan).asJsonObject.get("jadwalAdzan") as JsonObject
            val subuh = data["subuh"].asString
            val terbit = data["terbit"].asString
            val dzuhur = data["dzuhur"].asString
            val asar = data["asar"].asString
            val maghrib = data["maghrib"].asString
            val isya = data["isya"].asString
            val dataAdzan = arrayListOf(subuh, terbit, dzuhur, asar, maghrib, isya)
            Log.e("jadwalAdzan", "data ${Gson().toJson(dataAdzan)}")

            val currentTimestamp: String = SimpleDateFormat("HH:mm:ss").format(Date())
            val sdfTime = SimpleDateFormat("HH:mm:ss")

            //kirim notifikasi
            if (sdfTime.parse(subuh) == sdfTime.parse(currentTimestamp)) {
                sendNotification(context, intent, "subuh")
            } else if (sdfTime.parse(terbit) == sdfTime.parse(currentTimestamp)) {
                sendNotification(context, intent, "terbit")
            } else if (sdfTime.parse(dzuhur) == sdfTime.parse(currentTimestamp)) {
                sendNotification(context, intent, "dzuhur")
            } else if (sdfTime.parse(asar) == sdfTime.parse(currentTimestamp)) {
                sendNotification(context, intent, "asar")
            } else if (sdfTime.parse(maghrib) == sdfTime.parse(currentTimestamp)) {
                sendNotification(context, intent, "maghrib")
            } else if (sdfTime.parse(isya) == sdfTime.parse(currentTimestamp)) {
                sendNotification(context, intent, "isya")
            }
        }
    }

    private fun sendNotification(context: Context, itn: Intent, adzan: String) {
        val notif_title = "Sudah masuk waktu $adzan"
        val notif_content = "Solat dulu yuk."
        alarmNotificationManager = context
                .getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val newIntent = Intent(context, MainActivity::class.java)
        newIntent.putExtra("notifkey", "notifvalue")
        val contentIntent = PendingIntent.getActivity(context, 0,
                newIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_HIGH
            val mChannel = NotificationChannel(
                    NOTIFICATION_CHANNEL_ID, NOTIFICATION_CHANNEL_NAME, importance)
            alarmNotificationManager?.createNotificationChannel(mChannel)
        }

        val alamNotificationBuilder = NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
        alamNotificationBuilder.setContentTitle(notif_title)
        alamNotificationBuilder.setSmallIcon(R.drawable.ic_notification_overlay)
        alamNotificationBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
        alamNotificationBuilder.setContentText(notif_content)
        alamNotificationBuilder.setAutoCancel(true)
        alamNotificationBuilder.setContentIntent(contentIntent)
        alarmNotificationManager?.notify(NOTIFICATION_ID, alamNotificationBuilder.build())
    }


}