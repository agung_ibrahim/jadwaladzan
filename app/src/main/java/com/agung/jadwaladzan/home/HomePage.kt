package com.agung.jadwaladzan.home

import android.app.*
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.agung.jadwaladzan.DataModel
import com.agung.jadwaladzan.R
import com.agung.jadwaladzan.utils.BaseFragment
import com.agung.jadwaladzan.utils.MyService
import com.google.gson.Gson
import kotlinx.android.synthetic.main.home_layout.*
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.*


class HomePage : BaseFragment() {

    private var mAdapter: AdapterTanggal? = null
    private var timer: Timer? = null
    private var timerTask: TimerTask? = null
    private var counter = 0
    private var time = ""
    val dateFormat = SimpleDateFormat("dd-MM-yyyy")
    private var pendingIntent: PendingIntent? = null
    private val ALARM_REQUEST_CODE = 134

    private val interval_seconds = 1
    private val NOTIFICATION_ID = 1

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.home_layout, null, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    override fun onResume() {
        super.onResume()
        addData()
        stopService()
        startService()
    }

    fun init() {
        val alarmIntent = Intent(context, AppReceiver::class.java)
        pendingIntent = PendingIntent.getBroadcast(context, ALARM_REQUEST_CODE, alarmIntent, 0)

        mAdapter = AdapterTanggal(context!!, this)
        var layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        rv_calendar.layoutManager = layoutManager
        rv_calendar.adapter = mAdapter
    }

    fun addData() {
        val dataAdzan = DataModel.JadwalAdzan()
        dataAdzan.subuh = "04:10:00"
        dataAdzan.terbit = "05:29:00"
        dataAdzan.dzuhur = "11:37:00"
        dataAdzan.asar = "14:48:00"
        dataAdzan.maghrib = "17:46:00"
        dataAdzan.isya = "18:56:00"

        val dataAdzan1 = DataModel.JadwalAdzan()
        dataAdzan1.subuh = "04:11:00"
        dataAdzan1.terbit = "05:35:00"
        dataAdzan1.dzuhur = "11:38:00"
        dataAdzan1.asar = "14:55:00"
        dataAdzan1.maghrib = "17:59:00"
        dataAdzan1.isya = "18:50:00"

        val data0 = DataModel()
        data0.tanggal = "21-10-2020"
        data0.jadwalAdzan = dataAdzan

        val data1 = DataModel()
        data1.tanggal = "22-10-2020"
        data1.jadwalAdzan = dataAdzan

        val data2 = DataModel()
        data2.tanggal = "23-10-2020"
        data2.jadwalAdzan = dataAdzan

        val data3 = DataModel()
        data3.tanggal = "24-10-2020"
        data3.jadwalAdzan = dataAdzan

        val data4 = DataModel()
        data4.tanggal = "25-10-2020"
        data4.jadwalAdzan = dataAdzan

        val data5 = DataModel()
        data5.tanggal = "26-10-2020"
        data5.jadwalAdzan = dataAdzan

        val data6 = DataModel()
        data6.tanggal = "27-10-2020"
        data6.jadwalAdzan = dataAdzan

        val data7 = DataModel()
        data7.tanggal = "28-10-2020"
        data7.jadwalAdzan = dataAdzan1

        val data8 = DataModel()
        data8.tanggal = "29-10-2020"
        data8.jadwalAdzan = dataAdzan

        val data9 = DataModel()
        data9.tanggal = "30-10-2020"
        data9.jadwalAdzan = dataAdzan

        val data10 = DataModel()
        data10.tanggal = "01-11-2020"
        data10.jadwalAdzan = dataAdzan

        val list = arrayListOf(data0, data1, data2, data4, data3, data5, data6, data7, data8, data9, data10)

        val currentTimestamp: String = SimpleDateFormat("dd-MM-yyyy").format(Date())

        val filterList = list.filter { dateFormat.parse(it.tanggal.toString()).time >= dateFormat.parse(currentTimestamp).time }
        val finalList = filterList.sortedByDescending { dateFormat.parse(currentTimestamp).time - dateFormat.parse(it.tanggal.toString()).time }

        finalList[0].jadwalAdzan?.let { setAdzan(it) }
//        finalList[0].jadwalAdzan?.let { blink(it) }
        finalList[0].jadwalAdzan?.let { finalList[1].jadwalAdzan?.let { it1 -> blink(it, it1) } }
        mAdapter?.updateList(finalList)

        Log.e("data", "${Gson().toJson(finalList)}")

        val sp = context?.getSharedPreferences("cekData", Context.MODE_PRIVATE)
        sp?.edit()?.clear()?.apply()
        sp?.edit()?.putString("dataList","${Gson().toJson(finalList[0])}")?.apply()
        val nama = sp?.getString("dataList", "DEFAULT")
        Log.e("test","data sukses ${Gson().toJson(finalList[0])}")
        Toast.makeText(context, "TEST", Toast.LENGTH_SHORT).show()
    }

    private fun blink(data: DataModel.JadwalAdzan, data2: DataModel.JadwalAdzan) {
        val hander = Handler()
        Thread {
            try {
                Thread.sleep(550)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
            hander.post {
                val sdf = SimpleDateFormat("HH:mm")
                val sdfTime = SimpleDateFormat("HH:mm:ss")
                val timestamp = Timestamp(System.currentTimeMillis())
                val indonesia = Locale("id", "ID", "ID")
                val currentTimestamp: String = SimpleDateFormat("HH:mm:ss").format(Date())
                val test: String = SimpleDateFormat("HH:mm").format(Date())

                if (context != null) {
                    txt_timer.text = currentTimestamp
                    if (sdfTime.parse(data.subuh).time >= sdfTime.parse(currentTimestamp).time) {
                        txt_next_prayer.text = "Next Prayer Subuh ${data.subuh?.replace(":00", "")}"
                    } else if (sdfTime.parse(data.terbit).time >= sdfTime.parse(currentTimestamp).time) {
                        txt_next_prayer.text = "Next Prayer Terbit ${data.terbit?.replace(":00", "")}"
                    } else if (sdfTime.parse(data.dzuhur).time >= sdfTime.parse(currentTimestamp).time) {
                        txt_next_prayer.text = "Next Prayer Dzuhur ${data.dzuhur?.replace(":00", "")}"
                    } else if (sdfTime.parse(data.asar).time >= sdfTime.parse(currentTimestamp).time) {
                        txt_next_prayer.text = "Next Prayer Asar ${data.asar?.replace(":00", "")}"
                    } else if (sdfTime.parse(data.maghrib).time >= sdfTime.parse(currentTimestamp).time) {
                        txt_next_prayer.text = "Next Prayer Maghrib ${data.maghrib?.replace(":00", "")}"
                    } else if (sdfTime.parse(data.isya).time >= sdfTime.parse(currentTimestamp).time) {
                        txt_next_prayer.text = "Next Prayer Isya ${data.isya?.replace(":00", "")}"
                    } else {
                        txt_next_prayer.text = "Next Prayer (Tomorrow) Subuh ${data2.isya?.replace(":00", "")}"
                    }
                }

                blink(data, data2)
            }
        }.start()
    }

    fun setAdzan(data: DataModel.JadwalAdzan) {
        text_timer_subuh.text = data.subuh?.replace(":00", "")
        text_timer_terbit.text = data.terbit?.replace(":00", "")
        text_timer_dzuhur.text = data.dzuhur?.replace(":00", "")
        text_timer_asar.text = data.asar?.replace(":00", "")
        text_timer_maghrib.text = data.maghrib?.replace(":00", "")
        text_timer_isya.text = data.isya?.replace(":00", "")
    }

    fun startService() {
        val cal = Calendar.getInstance()
        cal.add(Calendar.SECOND, interval_seconds)
        val manager = context?.getSystemService(Context.ALARM_SERVICE) as AlarmManager?
        manager?.set(AlarmManager.RTC_WAKEUP, cal.timeInMillis, pendingIntent)
        Toast.makeText(context, "Start Service", Toast.LENGTH_SHORT).show()
    }

    fun stopService() {
        val manager = context?.getSystemService(Context.ALARM_SERVICE) as AlarmManager?
        manager!!.cancel(pendingIntent)
        val notificationManager = context?.getApplicationContext()?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancel(NOTIFICATION_ID)
        Toast.makeText(context, "Stop Service", Toast.LENGTH_SHORT).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        startService()
    }

    companion object {
        fun newInstance() : HomePage {
            return HomePage()
        }
    }
}