package com.agung.jadwaladzan

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.agung.jadwaladzan.home.HomePage
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when(item.itemId){
            R.id.navigation_home -> {
                val fragment = HomePage.newInstance()
                addFragment(fragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_shalat -> {
                null
            }
            R.id.navigation_quran -> {
                null
            }
            R.id.navigation_profile -> {
                null
            }
        }
        false
    }

    private fun addFragment(fragment: Fragment){
        supportFragmentManager.beginTransaction()
            .replace(R.id.content_page, fragment, fragment.javaClass.simpleName)
            .commit()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val fragment = HomePage.newInstance()
        addFragment(fragment)
        init()
    }


    fun init() {
        bottom_navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }
}